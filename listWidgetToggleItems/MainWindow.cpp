// -------------------------------------------------------------------------------------------------
//
// Copyright (C) 2015 HERE Global B.V. and/or its affiliated companies. All rights reserved.
//
// This software, including documentation, is protected by copyright controlled by
// HERE Global B.V. All rights are reserved. Copying, including reproducing, storing,
// adapting or translating, any or all of this material requires the prior written
// consent of HERE Global B.V. This material also contains confidential information,
// which may not be disclosed to others without prior written consent of HERE Global B.V.
//
// -------------------------------------------------------------------------------------------------

#include "MainWindow.h"
#include "ui_MainWindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    m_toggleAllItem = new QListWidgetItem(QString("Toggle All"),ui->listWidget);
    m_toggleAllItem->setCheckState(Qt::Unchecked);

    connect(ui->listWidget, &QListWidget::itemChanged, [=](QListWidgetItem* item) {
       if (item == m_toggleAllItem)  {

           auto checked = item->checkState();
           for (int i=0;i<ui->listWidget->count();++i) {

               auto item = ui->listWidget->item(i);
               item->setCheckState(checked);
           }
       }
    });

    for (int i=0;i<10;++i) {
        QListWidgetItem* item = new QListWidgetItem(QString("item%1").arg(i),ui->listWidget);
        item->setCheckState(Qt::Unchecked);
    }
}

MainWindow::~MainWindow()
{
    delete ui;
}
