#include <QCoreApplication>
#include <QDebug>
#include <QStringList>


/// Split csv line, with commas inside quotes ignored
QStringList splitCsvLine(const QString& line )
{

    QStringList list = line.split("\"", QString::SkipEmptyParts);
    QStringList res;

    bool inside = line[0]=='"';

    for( auto s: list) {

        if (inside) {
            res << s;
        } else {
            res << s.split(",", QString::SkipEmptyParts);
        }

        inside =!inside;
    }

    return res;
}


int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    QString line = "h,\"gg\",a,bb,\"c1,c2\",dd";
    auto res = splitCsvLine(line);

    for( auto s: res ){
        qDebug() << s;
    }

    return 0;
}
